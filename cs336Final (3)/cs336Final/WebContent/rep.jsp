<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>CS 336 Booking System</title>
	<style>
	*,html {
		margin: 0;
		padding: 0;
	}
	.body {
		color: #000;
	}
	.container {
		width: 250px;
		margin: 0 auto;
		padding-top: 100px;
		font-family: Arial, sans-serif;
	}
	.container h2 {
		text-align: center;	
	}
	#page {
		background-color: #8cc8ff;
		width: 250px;
		padding: 10px 20px 30px;
	}
	.tabs {
		width: 200px;
		border-collapse: collapse;
	}
	.cTab {
		background-color: #8cc8ff;
		padding: 10px 15px;
		width: 100px;
		cursor: pointer;
	}
	.eTab {
		background-color: #369fff;
		padding: 10px 15px;
		width: 100px;
		cursor: pointer;
	}
	.createAccountForm {
		text-align: center;
		padding-top: 20px;
	}
	.textInput {
		padding: 3px 5px;
		margin-left: 10px;
		margin-bottom: 5px;
	}
	#page .btn{
		margin-left: 180px;
	}
	.btn, .btn2 {
		background-color: #9c9c9c;
		color: #fff;
		padding: 5px;
		cursor: pointer;
	}
	</style>
</head>
<body>
	<div class="container">
		<h2>Welcome Customer Representative</h2>

	</div>
</body>
</html>